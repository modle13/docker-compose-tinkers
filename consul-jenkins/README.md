# consul docker-compose source

https://raw.githubusercontent.com/hashicorp/consul/master/demo/docker-compose-cluster/docker-compose.yml

# consul in the browser

localhost:8500

# jenkins in the browser

localhost:8080

# list containers

docker ps --format "table {{.Names}}\t{{.Ports}}"

# connect to a container

docker exec -it containername /bin/bash

# get jenkins admin pass

cat /var/jenkins_home/secrets/initialAdminPassword

# set authorization to anyone to allow plugins from jar
# there's probably a simple way to do this from the jenkins-cli.jar invocation itself to use the admin user

Admin > Configure Global Security > Authorization > Anyone can do anything

# install jenkins plugins from the commandline

java -jar $JENKINS_HOME/war/WEB-INF/jenkins-cli.jar -s http://127.0.0.1:8001 install-plugin consul


