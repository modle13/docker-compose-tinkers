# installation

https://docs.docker.com/compose/install

# tutorial

https://docs.docker.com/compose/gettingstarted/#step-5-edit-the-compose-file-to-add-a-bind-mount

# run

```
docker-compose up
```
